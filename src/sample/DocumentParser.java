package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DocumentParser {
    private static final String TAG = DocumentParser.class.getName();

    private String dataSource;
    private String tag;
    private boolean shouldLowercase;
    private boolean shouldCleanData;
    private Delegate delegate;

    public interface Delegate {
        void parseFinishedWithData(ObservableList<Model> data);
    }

    private DocumentParser(Delegate delegate) {
        this.delegate = delegate;
    }

    public static DocumentParser with(Delegate delegate) {
        return new DocumentParser(delegate);
    }

    public DocumentParser dataSource(String dataSource) {
        this.dataSource = dataSource;
        return this;
    }

    public DocumentParser tag(String tag) {
        this.tag = tag;
        return this;
    }

    public DocumentParser setShouldLowercase(boolean shouldLowercase) {
        this.shouldLowercase = shouldLowercase;
        return this;
    }

    public DocumentParser setShouldCleanData(boolean shouldCleanData) {
        this.shouldCleanData = shouldCleanData;
        return this;
    }

    public void start() {
        if (dataSource != null && !dataSource.isEmpty()) {
            Logger.d(TAG, "Starting parser.");

            String currentTag = tag;

            if (shouldLowercase) currentTag = tag.toLowerCase();

            Pattern p = Pattern.compile(String.format("<%s(.*?)</%s>", currentTag, currentTag));
            Matcher m = p.matcher(dataSource);

            int index = 0;

            ObservableList<Model> data = FXCollections.observableArrayList();

            while (m.find()) {
                index++;

                String currentData = m.group(1);

                if (shouldCleanData) {
                    currentData = currentData.split(">")[1];
                }

                data.add(new Model(index, currentData));
            }

            if (delegate != null) {
                delegate.parseFinishedWithData(data);
            }
        }
        else {
            Logger.d(TAG, "Data is invalid.");
        }
    }
}
