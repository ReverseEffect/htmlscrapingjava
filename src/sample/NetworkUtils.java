package sample;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class NetworkUtils {
    private static final String TAG = NetworkUtils.class.getName();

    public static String stringWithResource(String address) {
        try {
            URL url = new URL(address);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

            Scanner scn = new Scanner(connection.getInputStream());
            scn.useDelimiter("\\A");

            while (scn.hasNext()) {
                return scn.next();
            }
        }
        catch (Exception ex) {
            Logger.e(TAG, ex.getMessage());
        }

        return null;
    }
}
