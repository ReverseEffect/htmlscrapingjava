package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;

public class Controller implements DocumentParser.Delegate {

    private static final String TAG = Controller.class.getName();

    @FXML private TextField documentLocationTextField;
    @FXML private TextField tagTextField;
    @FXML private CheckBox stripCheckBox;
    @FXML private TableView<Model> dataTableView;
    @FXML private TableColumn<Model, Integer> indexColumn;
    @FXML private TableColumn<Model, String> dataColumn;

    @FXML private void parseButtonClicked() {
        String data = NetworkUtils.stringWithResource(documentLocationTextField.getText());
        // Logger.d(TAG, "Data > " + data);

        DocumentParser.with(this)
                .dataSource(data)
                .tag(tagTextField.getText())
                .setShouldLowercase(true)
                .setShouldCleanData(stripCheckBox.isSelected())
                .start();
    }

    @Override
    public void parseFinishedWithData(ObservableList<Model> data) {
        dataTableView.getItems().clear();

        indexColumn.setCellValueFactory(new PropertyValueFactory<>("index"));
        dataColumn.setCellValueFactory(new PropertyValueFactory<>("data"));

        dataTableView.setItems(data);

        Logger.d(TAG, "Model Length: " + data.size());
    }
}
